#!/usr/bin/env bats

export LAB_CORE_HOST={{ .Values.host }}
export LAB_CORE_USER={{ .Values.user }}
export LAB_CORE_TOKEN={{ .Values.token }}

mkdir -p /workspace
cd /workspace

setup () {
  git clone https://{{ .Values.user }}:{{ .Values.token }}@{{ .Values.repo }} test-pipeline
  cd test-pipeline
}

teardown () {
  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

@test "pipeline gets executed" {
  run lab ci create
  [ $status -eq 0 ]

  timeout 600 sh -c "until lab ci status | grep 'Pipeline Status: success'; do sleep 10; done"

  run lab ci status
  [ $status -eq 0 ]
  [[ "$output" =~ "Pipeline Status: success" ]]
}
