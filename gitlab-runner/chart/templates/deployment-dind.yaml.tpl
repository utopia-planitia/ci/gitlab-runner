apiVersion: apps/v1
kind: Deployment
metadata:
  name: dind
spec:
  selector:
    matchLabels:
      app: dind
  strategy:
    type: Recreate
  replicas: 1
  template:
    metadata:
      annotations:
        backup.velero.io/backup-volumes-excludes: data
      labels:
        app: dind
    spec:
      shareProcessNamespace: true
      {{- if .Values.services.lets_encrypt.additional_ca }}
      initContainers:
        - name: download-ca-certificates
          image: docker.io/library/docker:28.0.1-dind@sha256:71835c5efd0a0f907330f2b04b0ea8a143d0201072494e67eb7699d69281c9e2
          workingDir: /usr/local/share/ca-certificates
          args:
          command:
            - ash
            - -euxc
          args:
            - |
              URL={{ .Values.services.lets_encrypt.additional_ca }}
              FILE=$(basename "${URL:?}")
              if ! wget -c -O "${FILE:?}" "${URL:?}"; then
                rm "${FILE:?}"
                wget -O "${FILE:?}" "${URL:?}"
              fi
          volumeMounts:
            - mountPath: /usr/local/share/ca-certificates
              name: ca-certificates
      {{- end }}
      containers:
        - name: nurse
          image: ghcr.io/turbine-kreuzberg/dind-nurse:latest@sha256:eee8379a5767dd3dd65b974573e32412f06417e198c086ec40d247d251561852
          imagePullPolicy: Always
          args:
            - server
            - --upper-disk-usage-limit=85 # default: 90
            - --parallel-request-limit=99999
            - --dind-memory-limit=4194304000
            - --buildkitd-toml=/buildkitd/buildkitd.toml
          ports:
            - name: nurse
              containerPort: 2375
          resources:
            requests:
              memory: 200Mi
              cpu: 100m
            limits:
              memory: 200Mi
              cpu: 100m
          readinessProbe:
            httpGet:
              path: /_nurse_healthy
              port: 2375
          volumeMounts:
            - name: data
              mountPath: /var/lib/docker
            - name: buildkitd-config
              mountPath: /buildkitd
        - name: dind
          image: docker.io/library/docker:28.0.1-dind@sha256:71835c5efd0a0f907330f2b04b0ea8a143d0201072494e67eb7699d69281c9e2
          command:
            - ash
            - -euxc
          args:
            - |
              update-ca-certificates
              while true; do
                dockerd-entrypoint.sh dockerd \
                  --host=tcp://0.0.0.0:12375 \
                  --tls=false \
                  --mtu=1300 \
                  --ipv6=false \
                  --insecure-registry=10.0.0.0/8 \
                  --insecure-registry=172.16.0.0/12 \
                  --insecure-registry=192.168.0.0/16 \
                  &> /dev/null
                date;
                echo restarted to avoid OOM during builds;
              done
          env:
            - name: DOCKER_TLS_CERTDIR
              value: ""
            - name: DOCKER_BUILDKIT
              value: "1"
          securityContext: 
            privileged: true
          ports:
            - name: dind
              containerPort: 12375
          resources:
            requests:
              memory: 32Gi
              cpu: 8
            limits:
              memory: 32Gi
              cpu: 8
          readinessProbe:
            httpGet:
              path: /_ping
              port: 12375
          volumeMounts:
            - name: data
              mountPath: /var/lib/docker
            - mountPath: /usr/local/share/ca-certificates
              name: ca-certificates
      volumes:
        - name: data
          persistentVolumeClaim:
            claimName: dind
        - name: buildkitd-config
          configMap:
            name: buildkitd
        - name: ca-certificates
          emptyDir: {}
