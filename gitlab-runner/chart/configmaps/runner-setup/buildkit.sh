#!/bin/sh

# parse arguments
DOCKERFILE=./Dockerfile
CONTEXT=.
CACHE_REPO=
PLATFORM=
TARGET=
BUILD_ARGS=
for i in "$@"; do
    case $i in
        -t=*|--tag=*)
            IMAGE="${i#*=}"
            shift
            ;;
        -c=*|--context=*)
            CONTEXT="${i#*=}"
            shift
            ;;
        --cache-repo=*)
            CACHE_REPO="${i#*=}"
            shift
            ;;
        -f=*|--file=*)
            DOCKERFILE="${i#*=}"
            shift
            ;;
        --platform=*)
            PLATFORM="${i#*=}"
            shift
            ;;
        --target=*)
            TARGET="${i#*=}"
            shift
            ;;
        --build-arg=*)
            BUILD_ARGS="--opt build-arg:${i#*=} "
            shift
            ;;
        *)
            echo unknown option
            exit 1
            ;;
    esac
done

CONTEXT=$(realpath ${CONTEXT})

export IMAGE
export CONTEXT
export DOCKERFILE
export PLATFORM
export TARGET
export BUILD_ARGS

echo "image: ${IMAGE}"
echo "context: ${CONTEXT}"
echo "dockerfile: ${DOCKERFILE}"
echo "platform: ${PLATFORM}"
echo "target: ${TARGET}"
echo "build args: ${BUILD_ARGS}"

if [ "${PLATFORM}" != "" ]; then
    PLATFORM="--opt platform=${PLATFORM}"
fi

if [ "${TARGET}" != "" ]; then
    TARGET="--opt target=${TARGET}"
fi

if [ "${CACHE_REPO}" = "" ]; then
    CACHE_REPO="${DOCKER_REGISTRY_INTERNAL}/${CI_PROJECT_PATH}:cache"
fi

DOCKERFILE_DIR=$(dirname ${DOCKERFILE})
DOCKERFILE_FILE=$(basename ${DOCKERFILE})

export BUILDCTL_CONNECT_RETRIES_MAX=100
CMD="buildctl-daemonless.sh \
      build \
      --frontend dockerfile.v0 \
      --local context=${CONTEXT} \
      --local dockerfile=${DOCKERFILE_DIR} \
      --opt filename=${DOCKERFILE_FILE} \
      ${PLATFORM} \
      ${TARGET} \
      ${BUILD_ARGS} \
      --output type=image,push=true,name=${IMAGE} \
      --export-cache=type=registry,ref=${CACHE_REPO},mode=max \
      --import-cache=type=registry,ref=${CACHE_REPO}"

if ! sh -c "${CMD}"; then
    echo "build or push to registry failed"
    echo "re-executing build"
    exec ${CMD}
fi
