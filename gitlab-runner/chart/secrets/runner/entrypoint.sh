#!/usr/bin/env bash

set -euxo pipefail

apt-get update

DEBIAN_FRONTEND=noninteractive apt-get install --assume-yes --no-install-recommends gettext-base

envsubst \
	'$CACHE_S3_ACCESS_KEY $CACHE_S3_SECRET_KEY $CACHE_S3_BUCKET_NAME $CACHE_S3_BUCKET_HOST' \
	</etc/gitlab-runner/config.toml.template \
	>/etc/gitlab-runner/config.toml

exec /entrypoint "${@}"
