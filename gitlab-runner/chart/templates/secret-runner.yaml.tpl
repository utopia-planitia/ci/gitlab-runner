apiVersion: v1
kind: Secret
metadata:
  name: runner
type: Opaque
stringData:
{{ (tpl (.Files.Glob "secrets/runner/*").AsConfig . ) | indent 2 }}
