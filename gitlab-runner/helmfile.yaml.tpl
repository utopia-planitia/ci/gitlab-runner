releases:
  - name: gitlab-runner
    namespace: gitlab-runner
    chart: ./chart
    values:
      - cluster:
          domain: {{ .Values.cluster.domain }}
          name: {{ .Values.cluster.name }}
          password: {{ .Values.cluster.password }}
          basic_auth: {{ .Values.cluster.basic_auth }}
        services:
          minio:
            access_key: {{ .Values.services.minio.access_key }}
            secret_key: {{ .Values.services.minio.secret_key }}
          lets_encrypt:
            additional_ca: |-
              {{ or
                (.Values | dig "services" "lets_encrypt" "additional_ca" "")
                (.Values | dig "services" "lets_encrypt" "additionalCA" "")
              }}
        runners:
{{ .Values.services.gitlab_runner.runners | toYaml | indent 8 }}
