apiVersion: v1
kind: Secret
metadata:
  name: "helmfile-test-scripts"
type: Opaque
stringData:
{{ (tpl (.Files.Glob "test-scripts/*").AsConfig . ) | indent 2 }}
