apiVersion: v1
kind: ConfigMap
metadata:
  name: runner-setup
data:
{{ (.Files.Glob "configmaps/runner-setup/*").AsConfig | indent 2 }}
