kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: gitlab-ci-runner
subjects:
  - kind: ServiceAccount
    name: default
    namespace: "{{ .Release.Namespace }}"
roleRef:
  kind: ClusterRole
  name: cluster-admin
  apiGroup: rbac.authorization.k8s.io
