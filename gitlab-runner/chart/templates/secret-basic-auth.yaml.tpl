apiVersion: v1
kind: Secret
metadata:
  name: basic-auth
type: Opaque
stringData:
  auth: "{{ .Values.cluster.basic_auth }}"
