#!/bin/sh

if test -n "${INSTALL_LETS_ENCRYPT_STAGE_CERT:-}"; then
	echo "INFO: Installing the Let's Encrypt staging certificates ..." >&2
	if ! command -v curl >/dev/null && ! command -v wget >/dev/null; then
		if command -v apt-get >/dev/null; then
			apt-get update >/dev/null
			apt-get install --assume-yes --no-install-recommends \
				curl \
				>/dev/null
		elif command -v apk >/dev/null; then
			apk update >/dev/null
			apk add \
				curl \
				>/dev/null
		fi
	fi
	mkdir -p /usr/local/share/ca-certificates
	# https://letsencrypt.org/docs/staging-environment/
	for UTOPIA_PRE_BUILD_SCRIPT_CERTIFICATE in \
		letsencrypt-stg-root-x2.pem \
		letsencrypt-stg-root-x1.pem; do
		if command -v curl >/dev/null; then
			curl \
				--fail \
				--location \
				--output "/usr/local/share/ca-certificates/${UTOPIA_PRE_BUILD_SCRIPT_CERTIFICATE:?}" \
				--show-error \
				--silent \
				"https://letsencrypt.org/certs/staging/${UTOPIA_PRE_BUILD_SCRIPT_CERTIFICATE:?}" >/dev/null
		elif command -v wget >/dev/null; then
			wget -qO \
				"/usr/local/share/ca-certificates/${UTOPIA_PRE_BUILD_SCRIPT_CERTIFICATE:?}" \
				"https://letsencrypt.org/certs/staging/${UTOPIA_PRE_BUILD_SCRIPT_CERTIFICATE:?}" >/dev/null
		fi
	done
	if ! command -v update-ca-certificates >/dev/null; then
		if command -v apt-get >/dev/null; then
			apt-get update >/dev/null
			apt-get install --assume-yes --no-install-recommends \
				ca-certificates \
				>/dev/null
		elif command -v apk >/dev/null; then
			apk update >/dev/null
			apk add \
				ca-certificates \
				>/dev/null
		fi
	fi
	if command -v update-ca-certificates >/dev/null; then
		update-ca-certificates >/dev/null
	fi
fi

# maven (java)
mkdir -p "${HOME:?}/.m2"
cp /gitlab-runner-setup/maven-settings.xml "${HOME:?}/.m2/settings.xml"

# npm (javascript)
if command -v npm >/dev/null; then
	npm config set registry http://npm-registry.npm-registry.svc --global
fi

# minio client
if command -v mc >/dev/null && test -n "${MINIO_PUBLIC_URL:-}" && test -n "${MINIO_ACCESS_KEY:-}" && test -n "${MINIO_SECRET_KEY:-}"; then
	mc config host add minio "${MINIO_PUBLIC_URL:?}" "${MINIO_ACCESS_KEY:?}" "${MINIO_SECRET_KEY:?}"
	if test -n "${MINIO_BUCKET:-}"; then
		mc mb "minio/${MINIO_BUCKET:?}" --ignore-existing
	fi
fi

# tilt
if command -v tilt >/dev/null && test "${TILT_NAMESPACE:-}" != ""; then
	kubectl create ns "${TILT_NAMESPACE:?}" --dry-run=client -o yaml | kubectl apply -f -
	mkdir -p ~/.kube
	kubectl config set-cluster ci --server="https://${KUBERNETES_SERVICE_HOST:?}:${KUBERNETES_SERVICE_PORT:?}" --certificate-authority=/run/secrets/kubernetes.io/serviceaccount/ca.crt
	kubectl config set-credentials ci --token="$(cat /run/secrets/kubernetes.io/serviceaccount/token || true)"
	kubectl config set-context ci --cluster=ci --user=ci --namespace="${TILT_NAMESPACE:?}"
	kubectl config use-context ci
fi

# composer (php package manager)
if command -v composer >/dev/null; then
	if composer --version --no-ansi | grep -E "version 2\."; then
		if test "${DISABLE_AUTOMATIC_PHP_PACKAGE_CACHE:-}" = ""; then
			if test -f composer.json; then
				if composer config --global allow-plugins >/dev/null 2>&1; then
					composer global config --no-interaction -- allow-plugins.repman-io/composer-plugin true
				fi
				composer global require --no-interaction repman-io/composer-plugin
				if command -v jq >/dev/null; then
					jq '. += {"config":{"secure-http": false},"extra": {"repman": {"url": "http://php-package-cache.php-package-cache.svc:8080/"}}}' composer.json \
						>composer.json.tmp &&
						mv composer.json.tmp composer.json
				else
					echo "please add jq to enable auto setup of the local php package cache"
				fi
			fi
		fi
	else
		echo "please update to composer 2.0 to use the local php package cache"
	fi
fi

# registry login for skopeo
if command -v skopeo >/dev/null; then
	echo "INFO: Logging in to \"${DOCKER_REGISTRY_EXTERNAL:?}\" using Skopeo ..." >&2
	skopeo login --username "${DOCKER_REGISTRY_USERNAME:?}" --password "${DOCKER_REGISTRY_PASSWORD:?}" "${DOCKER_REGISTRY_EXTERNAL:?}"
fi

# registry login for docker
if command -v docker >/dev/null; then
	echo "INFO: Logging in to \"${DOCKER_REGISTRY_EXTERNAL:?}\" using Docker ..." >&2
	echo "${DOCKER_REGISTRY_PASSWORD:?}" | docker login --password-stdin --username "${DOCKER_REGISTRY_USERNAME:?}" "${DOCKER_REGISTRY_EXTERNAL:?}"
fi

# registry login for buildkitd
if command -v buildkitd >/dev/null; then
	echo "INFO: Creating BuildKit auth config for the registry \"${DOCKER_REGISTRY_EXTERNAL:?}\" ..." >&2
	if test -f ~/.docker/config.json; then
		echo "WARN: Skipping container registry login for \"${DOCKER_REGISTRY_EXTERNAL:?}\" because the file \"~/.docker/config.json\" already exists." >&2
	else
		mkdir -p ~/.docker
		DOCKER_LOGIN=$(printf '%s:%s' "${DOCKER_REGISTRY_USERNAME:?}" "${DOCKER_REGISTRY_PASSWORD:?}" | base64)
		printf '{"auths":{"%s":{"auth":"%s"}}}' "${DOCKER_REGISTRY_EXTERNAL:?}" "${DOCKER_LOGIN:?}" >~/.docker/config.json
	fi
fi

UTOPIA_PRE_BUILD_SCRIPT_USER_ID=$(id -u)
if test "${UTOPIA_PRE_BUILD_SCRIPT_USER_ID:?}" -eq "0"; then
	UTOPIA_PRE_BUILD_SCRIPT_BUILDKITD_TOML=/etc/buildkit/buildkitd.toml
else
	UTOPIA_PRE_BUILD_SCRIPT_BUILDKITD_TOML=~/.config/buildkit/buildkitd.toml
fi

# configuration for buildkitd or docker buildx
if command -v buildkitd >/dev/null || (command -v docker >/dev/null && docker buildx version >/dev/null 2>&1); then
	if test -f "${UTOPIA_PRE_BUILD_SCRIPT_BUILDKITD_TOML:?}"; then
		echo "Skipping buildkitd configuration for \"${DOCKER_REGISTRY_INTERNAL:?}\" because the file \"${UTOPIA_PRE_BUILD_SCRIPT_BUILDKITD_TOML:?}\" already exists." >&2
	else
		mkdir -p "$(dirname "${UTOPIA_PRE_BUILD_SCRIPT_BUILDKITD_TOML:?}")"
		printf '[registry."docker.io"]\n  mirrors = ["mirror.gcr.io", "registry-1.docker.io"]\n\n[registry."%s"]\n  http = true\n  insecure = true\n\n' "${DOCKER_REGISTRY_INTERNAL:?}" >"${UTOPIA_PRE_BUILD_SCRIPT_BUILDKITD_TOML:?}"
	fi
fi

# docker buildx builder
if command -v docker >/dev/null && docker buildx version >/dev/null 2>&1; then
	if test -f "${UTOPIA_PRE_BUILD_SCRIPT_BUILDKITD_TOML:?}"; then
		docker buildx create --name ci-builder --config="${UTOPIA_PRE_BUILD_SCRIPT_BUILDKITD_TOML:?}"
		docker buildx use --default --builder ci-builder
	else
		echo "Skipping creation of a docker buildx builder instance because the file \"${UTOPIA_PRE_BUILD_SCRIPT_BUILDKITD_TOML:?}\" does not exist." >&2
	fi
fi

if test "${CI_JOB_NAME:?}" = "${SLEEP_JOB_NAME:-}" && test "${CI_PIPELINE_ID:?}" = "${SLEEP_PIPELINE_ID:-}"; then
	echo sleep "${SLEEP_PRE_TIME:?}"
	sleep "${SLEEP_PRE_TIME:?}"
fi
